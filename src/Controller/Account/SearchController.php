<?php

namespace App\Controller\Account;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;


/** 
 * @Route("/account", name="account_") 
 */
class SearchController extends AbstractController
{
    /**
     * @Route("/search", name="search")
     */
    public function search(Request $request)
    {
        $filters    = $request->query->all();
        $q          = isset($filters['q']) ? $filters['q'] : null;
        

        return $this->render('Account/dashboard/search.html.twig', [
            'q' => $q,
            
        ]);
    }
}