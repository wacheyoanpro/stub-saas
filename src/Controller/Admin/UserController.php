<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AdminBaseController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

use Ramsey\Uuid\Uuid;

use App\Validator\Admin\UserValidator;
use App\Service\MailerService;
use App\Entity\Account;
use App\Entity\User;
use App\Entity\UserNote;

/** 
 * @Route("/admin", name="admin_") 
 */
class UserController extends AdminBaseController
{
    private $userValidator;
    private $params;

    public function __construct(UserValidator $userValidator, MailerService $mailer, ParameterBagInterface $params)
    {
        $this->userValidator = $userValidator;
        $this->params        = $params;
        $this->mailer        = $mailer;
    }

    /**
     * @Route("/user/list", name="user_list")
     */
    public function userList()
    {
        $repo           = $this->getDoctrine()->getRepository(User::class);
        $total          = $repo->countAll();
        $totalActive    = $repo->countActive();
        $users          = $repo->findAll();

        return $this->render('Admin/account/userList.html.twig', [
            'total'     => $total,
            'active'    => $totalActive,
            'users'     => $users
        ]);
    } 


    /**
     * @Route("/account/{account}/user/list", name="account_user_list")
     */ 
    public function accountUserList(Account $account)
    {
        $repo           = $this->getDoctrine()->getRepository(User::class);
        $total          = $repo->countAllByAccount($account);
        $totalActive    = $repo->countActiveByAccount($account);
        $users          = $repo->findAllByAccount($account);

    
        return $this->render('Admin/account/userList.html.twig', [
            'total'     => $total,
            'active'    => $totalActive,
            'users'     => $users,
            'account'   => $account,
        ]);
    }

    /**
     * @Route("/account/{account}/user/{user}/edit", name="account_user_edit")
     */
    public function userEdit(Account $account, User $user, Request $request)
    {
        if ($user->getAccount()->getId() != $account->getId()) {
            throw new AccessDeniedException('Admin Access Required');
        }

        if ($request->isMethod("POST")) {
            $errors      = null;
            $data        = $request->request->all();
            $userRepo    = $this->getDoctrine()->getRepository(User::class);
            $accountRepo = $this->getDoctrine()->getRepository(Account::class);

            $result = $this->userValidator->userEditValidate($user, $data);
            if (!empty($result['errors'])) {
                foreach ($result['errors'] as $err) {
                    foreach($err as $label => $mesg) {
                        $this->addFlash($label, $mesg);
                    }
                }
            } else {
                $this->addFlash('success', 'User information updated');
                $newUser = $result['user'];

                // TODO Add change log of which user updated this info, from what IP, at what time, and what changed (store original data as json)
                $this->getDoctrine()->getManager()->persist($newUser);
                $this->getDoctrine()->getManager()->flush();
            }
        }

        return $this->render('Admin/account/userEdit.html.twig', [
            'u' => $user,
            'roles' => ['ROLE_USER', 'ROLE_ADMIN'],
        ]);
    }

    /**
     * @Route("/account/{account}/user/{user}/delete", name="account_user_del", methods={"GET"})
     */
    public function userDelete(Account $account, User $user)
    {   
        $user->setIsDeleted(true);

        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('success', 'Account marked for deletion');

        return $this->redirectToRoute('admin_account_user_edit',['account' => $account->getId(), 'user' => $user->getId()]);
    }

     /**
     * @Route("/account/{account}/user/{user}/undelete", name="account_user_undel", methods={"GET"})
     */
    public function userUnDelete(Account $account, User $user)
    {   
        $user->setIsDeleted(false);

        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('success', 'Account undeleted');

        return $this->redirectToRoute('admin_account_user_edit',['account' => $account->getId(), 'user' => $user->getId()]);
    }

    /**
     * @Route("/account/{account}/user/{user}/note/add", name="account_user_note_add", methods={"POST"})
     */
    public function addNote(Account $account, User $user, Request $request)
    {
        $data       = $request->request->all();
        $mesg       = isset($data['note']) ? trim($data['note']) : null;
        $followup   = isset($data['followup']) && $data['followup'] == 1 ? true : false;

        if (empty($mesg)) {
            $this->addFlash('danger', 'Can\'t add an empty note');
        } else {
            $this->addFlash('success', 'Note added');
            
            $note = new UserNote();
            $note->setCreatedBy($this->getUser());
            $note->setCreatedAt(new \DateTime());
            $note->setNote($mesg);
            $note->setRequireFollowUp($followup);
            $note->setUser($user);

            $this->getDoctrine()->getManager()->persist($note);
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->redirectToRoute('admin_account_user_edit', ['account' => $account->getId(), 'user' => $user->getId()]);
    }

    /**
     * @Route("/account/{account}/user/usernameCheck/{username}", name="account_username_check")
     */
    public function usernameCheck($username, Request $request)
    {
        $result = $this->userValidator->validateUsername($username);

        return $this->sendJson(['data' => $result]);
    }

    /**
     * @Route("/account/{account}/user/emailCheck/{email}", name="account_email_check")
     */
    public function emailCheck($email, Request $request)
    {
        $result = $this->userValidator->validateEmail($email);

        return $this->sendJson(['data' => $result]);
    }

    /**
     * @Route("/account/{account}/user/add", name="account_user_add", methods={"POST"})
     */
    public function userAdd(Account $account, Request $request)
    {
        $data = $request->request->all();
        $data['account'] = $account->getId();
        $data['isVerified'] = 1;
        $data['isActive'] = 1;

        $user = new User();
        $result = $this->userValidator->userEditValidate($user, $data);

        if (!empty($result['errors'])) {
            foreach ($result['errors'] as $err) {
                foreach($err as $label => $mesg) {
                    $this->addFlash($label, $mesg);
                }
            }
        } else {
            $this->addFlash('success', 'User Created');

            $ttl  = $this->params->get('password_token_ttl');
            $newUser = $result['user'];

            // Field cannot be blank in database. Non-hashed password won't allow for login
            $newUser->setPassword(\uniqid());
            $newUser->setIsDeleted(false);

            // Generate a lost password token
            $token = Uuid::uuid4();
            $newUser->setPasswordToken($token);

            $date = new \DateTime();
            $date->add(new \DateInterval('PT'.$ttl.'H'));
            $newUser->setTokenExpiresAt($date);
            
            $this->getDoctrine()->getManager()->persist($newUser);
            $this->getDoctrine()->getManager()->flush();

            $this->mailer->sendNewUserEmail($user);
        }

        return $this->redirectToRoute('admin_account_user_list', ['account' => $account->getId()]);
    }
}
