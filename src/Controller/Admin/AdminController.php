<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AdminBaseController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Service\MailerService;
use App\Validator\Admin\AccountValidator;

use App\Entity\Account;
use App\Entity\User;
use App\Entity\AccountNote;
use App\Entity\AccountFeature;


/** 
 * @Route("/admin", name="admin_") 
 */
class AdminController extends AdminBaseController
{
    private $mailer;
    private $accountValidator;

    public function __construct(AccountValidator $accountValidator, MailerService $mailer)
    {
        $this->mailer = $mailer;
        $this->accountValidator = $accountValidator;
    }

    /**
     * @Route("/", name="main")
     */
    public function index()
    {
        return $this->render('Admin/index.html.twig', [

        ]);
    }

    /**
     * @Route("/account/list", name="account_list")
     */
    public function accountList()
    {
        $repo           = $this->getDoctrine()->getRepository(Account::class);
        $total          = $repo->countAll();
        $totalActive    = $repo->countActive();
        $accounts       = $repo->findBy([], ['createdAt' => 'DESC']);
    
        return $this->render('Admin/account/list.html.twig', [
            'total'     => $total,
            'active'    => $totalActive,
            'accounts'  => $accounts
        ]);
    }

    /**
     * @Route("/account/{account}/edit", name="account_edit")
     */
    public function accountDetail(Account $account, Request $request)
    {
        $repo           = $this->getDoctrine()->getRepository(Account::class);
        $account        = $repo->findOneById($account);
        $featureRepo    = $this->getDoctrine()->getRepository(AccountFeature::class);
        $features       = $featureRepo->featureByAccount($account);

        if ($request->isMethod("POST")) {
            $data   = $request->request->all();
            $result = $this->accountValidator->accountValidate($account, $data);
            
            if (!empty($result['errors'])) {
                foreach ($result['errors'] as $err) {
                    foreach($err as $label => $mesg) {
                        $this->addFlash($label, $mesg);
                    }
                }
            } else {
                $this->addFlash('success', 'Account Updated');
    
                $newAccount = $result['account'];
    
                $this->getDoctrine()->getManager()->persist($newAccount);
                $this->getDoctrine()->getManager()->flush();
            }
    
            return $this->redirectToRoute('admin_account_edit', ['account' => $account->getId()]);
        }

        return $this->render('Admin/account/accountEdit.html.twig', [
            'account'  => $account,
            'features' => $features,
        ]);
    }


    /**
     * @Route("/account/create", name="account_create")
     */
    public function accountCreate(Request $request)
    {
        $data = $request->request->all();
        
        $account    = new Account();
        $result     = $this->accountValidator->accountValidate($account, $data);

        if (!empty($result['errors'])) {
            foreach ($result['errors'] as $err) {
                foreach($err as $label => $mesg) {
                    $this->addFlash($label, $mesg);
                }
            }
        } else {
            $this->addFlash('success', 'Account Created');

            $newAccount = $result['account'];

            $newAccount->setCreatedAt(new \DateTime());
            $newAccount->setIsDeleted(false);
            $newAccount->setIsActive(true);
            
            $this->getDoctrine()->getManager()->persist($newAccount);
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->redirectToRoute('admin_account_list');
    }


     /**
     * @Route("/account/{account}/note/add", name="account_note_add", methods={"POST"})
     */
    public function addNote(Account $account, Request $request)
    {
        $data       = $request->request->all();
        $mesg       = isset($data['note']) ? trim($data['note']) : null;

        if (empty($mesg)) {
            $this->addFlash('danger', 'Can\'t add an empty note');
        } else {
            $this->addFlash('success', 'Note added');
            
            $note = new AccountNote();
            $note->setCreatedBy($this->getUser());
            $note->setCreatedAt(new \DateTime());
            $note->setNote($mesg);
            $note->setAccount($account);

            $this->getDoctrine()->getManager()->persist($note);
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->redirectToRoute('admin_account_edit', ['account' => $account->getId()]);
    }
}
