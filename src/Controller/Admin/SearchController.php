<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AdminBaseController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\User;
use App\Entity\Account;

/** 
 * @Route("/admin", name="admin_") 
 */
class SearchController extends AdminBaseController
{
    /**
     * @Route("/search", name="search")
     */
    public function search(Request $request)
    {
        $filters    = $request->query->all();
        $userRepo   = $this->getDoctrine()->getRepository(User::class);
        $q          = isset($filters['q']) ? $filters['q'] : null;
        $users      = null;

        if (!empty($q) && \strlen($q) >= 3) {
            $userResults    = $userRepo->adminSearch($q);
            $userIds        = [];
            $count          = count($userResults);

            if (is_array($userResults) && $count > 0) {
                for ($i = 0; $i < $count; $i++) {
                    $userIds[] = $userResults[$i]['id'] ;
                }
            }
            $users = $userRepo->findBy(['id' => $userIds]);
        }
        
        return $this->render('Admin/account/search.html.twig', [
            'q'         => $q,
            'results'   => $users,
        ]);
    }
}