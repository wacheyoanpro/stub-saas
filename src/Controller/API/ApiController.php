<?php

namespace App\Controller\API;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;


/** 
 * @Route("/api", name="api_") 
 */
class ApiController extends AbstractController
{
    /**
     * @Route("/index", name="index")
     */
    public function index()
    {
        
        return $this->sendJson(['Your credentials work']);
    }

    /**
     * @param array|object $data 
     */
    public function sendJson($data, $statusCode = 200)
    {
        $message = null;
        $headers =[
            'X-Powered-By' => 'Hopes and Dreams',
            'X-Spirit-Animal' => 'Honeybadger'];

        $response = new JsonResponse($data, $statusCode, $headers);
        
        return $response;
    }
}
