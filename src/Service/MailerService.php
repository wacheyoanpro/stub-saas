<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

use App\Entity\User;


class MailerService 
{
    private $params;
    private $email;
    private $apiKey;
    private $mailingAddress;
    private $router;
    private $tokenLink;
    private $tokenTTL;
    private $name;
    private $toAddress;
    private $subject;

    public function __construct(Environment $twig, UrlGeneratorInterface $router, ParameterBagInterface $params)
    {
        $this->router               = $router;
        $this->params               = $params;
        $this->twig                 = $twig;
        $this->email                = new \SendGrid\Mail\Mail();
        $this->apiKey               = $this->params->get('sendgrid_api_key');
        $this->mailingAddress       = $this->params->get('physical_mailing_address');
        $this->siteName             = $this->params->get('site_name');
        $this->mailerFromAddress    = $this->params->get('mailer_from_address');
        $this->tokenTTL             = $this->params->get('password_token_ttl');
    }

    /**
     * @param object $user 
     * 
     * Sends the user an email on how to reset their password
     */
    public function sendForgotPassword(User $user)
    {
        $this->toAddress    = $user->getEmail();
        $this->subject      = "Your ". $this->siteName ." Password Reset Request";
        $tokenLink          = $this->router->generate(
                                    'app_changepass', 
                                    ['token' => $user->getPasswordToken()], 
                                    UrlGeneratorInterface::ABSOLUTE_URL
                                );        
        $this->getName($user);

        $htmlContent = $this->twig->render('Web/Email/forgotPassword.html.twig', [
            'name'              => $this->name,
            'subject'           => $this->subject,
            'tokenLink'         => $tokenLink,
            'tokenTTL'          => $this->tokenTTL,
            'mailingAddress'    => $this->mailingAddress,
        ]);

        $textContent = $this->twig->render('Web/Email/forgotPassword-text.html.twig', [
            'name'      => $this->name,
            'tokenLink' => $this->tokenLink,
            'tokenTTL'  => $this->tokenTTL,
        ]);
        
        $this->send($htmlContent, $textContent);
    }

    /**
     * @param object $user 
     * 
     * Sends a welcome email
     */
    public function sendNewUserEmail(User $user)
    {
        $this->toAddress    = $user->getEmail();
        $this->subject      = "Your ". $this->siteName ." New User Account";
        $tokenLink          = $this->router->generate(
                                'app_changepass', 
                                ['token' => $user->getPasswordToken()], 
                                UrlGeneratorInterface::ABSOLUTE_URL);
        $this->getName($user);

        $htmlContent = $this->twig->render('Web/Email/newUser.html.twig', [
            'name'              => $this->name,
            'subject'           => $this->subject,
            'tokenLink'         => $tokenLink,
            'tokenTTL'          => $this->tokenTTL,
            'mailingAddress'    => $this->mailingAddress,
            'mailerFromAddress' => $this->mailerFromAddress,
        ]);

        $textContent = $this->twig->render('Web/Email/newUser-text.html.twig', [
            'name'      => $this->name,
            'tokenLink' => $this->tokenLink,
            'tokenTTL'  => $this->tokenTTL,
            'mailerFromAddress' => $this->mailerFromAddress,
        ]);
        
        $this->send($htmlContent, $textContent);
    }


    /**
     * @param twig rendered html content
     * @param twig rendered text content
     * @return void
     * 
     * Sends the email
     */
    private function send($htmlContent, $textContent)
    {
        $this->email->setFrom($this->mailerFromAddress, $this->siteName);
        $this->email->setSubject($this->subject);
        $this->email->addTo($this->toAddress, $this->name);
        $this->email->addContent("text/plain", $textContent);
        $this->email->addContent("text/html", $htmlContent);
        $sendgrid = new \SendGrid($this->apiKey);

        try {
            $response   = $sendgrid->send($this->email);
            $statusCode = $response->statusCode();
            $headers    = $response->headers();
            $body       = $response->body();

        } catch (Exception $e) {
            // TODO - need to log this
            $errorMesg = $e->getMessage();
        }
    }

    /**
     * @param object $user
     * 
     * Determines the name to use in the email
     */
    private function getName(User $user)
    {
        if (!empty($user->getFirstName()) && !empty($user->getLastName())) {
            $this->name = $user->getFirstName() . ' ' . $user->getLastName();
        } else {
            $this->name = $user->getEmail();
        }
    }

    /**
     * If you have your SendGrid account setup and the environment variable set
     * with your API key, then calling this method should return a status code 202
     */
    public function test()
    {
        $email = new \SendGrid\Mail\Mail(); 
        $email->setFrom("test@example.com", "Example User");
        $email->setSubject("Sending with SendGrid is Fun");
        $email->addTo("test@example.com", "Example User");
        $email->addContent("text/plain", "and easy to do anywhere, even with PHP");
        $email->addContent(
            "text/html", "<strong>and easy to do anywhere, even with PHP</strong>"
        );
        $sendgrid = new \SendGrid($this->apiKey);
        try {
            $response = $sendgrid->send($email);
            print $response->statusCode() . "\n";
            print_r($response->headers());
            print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }
    }
}