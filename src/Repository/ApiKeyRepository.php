<?php

namespace App\Repository;

use App\Entity\ApiKey;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ApiKey|null find($id, $lockMode = null, $lockVersion = null)
 * @method ApiKey|null findOneBy(array $criteria, array $orderBy = null)
 * @method ApiKey[]    findAll()
 * @method ApiKey[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApiKeyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ApiKey::class);
    }

     /**
     * @param string $apiToken
     * @see TokenAuthenticator
     * 
     * Similar checks as loginUser but also makes sure
     * that the feature table has API enabled
     * and that the token is valid
     */
    public function loginUserApi(string $apiToken)
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.token = :key')
            
            ->leftJoin('k.user', 'u')
            ->andWhere('u.isDeleted = 0')
            ->andWhere('u.isActive = 1')
            ->andWhere('u.isVerified = 0')
            
            ->leftJoin('u.account', 'a')
            ->andWhere('a.isActive = 1')
            ->andWhere('a.isDeleted = 0')
            
            ->leftJoin('a.accountFeatures', 'f')
            ->andWhere('f.name = :enabled')
            ->andWhere('f.value = 1')
            
            ->setParameter('key', $apiToken)
            ->setParameter('enabled', 'apiEnabled')
            ->getQuery()
            ->getOneOrNullResult();
    }
}
