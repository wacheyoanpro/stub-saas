<?php

namespace App\Validator\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

use App\Entity\User;
use App\Entity\Account;

class UserValidator 
{
    private $em;
    private $userRepo;
    private $accountRepo;
    private $params;

    public function __construct(EntityManagerInterface $em, ParameterBagInterface $params)
    {
        $this->params       = $params;
        $this->em           = $em;
        $this->userRepo     = $this->em->getRepository(\App\Entity\User::class);
        $this->accountRepo  = $this->em->getRepository(\App\Entity\Account::class);
    }

    /**
     * Take an array of POST data, validate it, build error messages
     * Prep the user object for changes
     * 
     * @param array $data
     * @param object $user 
     * @return array $result
     */
    public function userEditValidate(User $user, array $data): array 
    {
        $errors     = [];
        $email      = isset($data['email'])     ? trim($data['email']) : null;
        $accountId  = isset($data['account'])   ? trim($data['account']) : null;
        $role       = isset($data['role'])      ? trim($data['role']) : null;
        $firstName  = isset($data['firstName']) ? trim($data['firstName']) : null;
        $lastName   = isset($data['lastName'])  ? trim($data['lastName']) : null;

        if (!isset($data['isActive']) || !is_numeric($data['isActive'])) {
            $errors[] = ['danger' => 'Status is required'];
        } else {
            $user->setIsActive($data['isActive']);
        }
        
        if (!isset($data['isVerified']) || !is_numeric($data['isVerified'])) {
            $errors[] = ['danger' => 'Verified is required'];
        } else {
            $user->setIsVerified($data['isVerified']);
        }

        if (empty($role)) {
            $errors[] = ['danger' => 'Role is required'];
        } elseif (!in_array($data['role'], ['ROLE_USER', 'ROLE_ADMIN'])) {
            $errors[] = ['danger' => 'Invalid role selected'];
        } else {
            $user->setRoles([trim($data['role'])]);
        }
        
        if (empty($accountId)) {
            $errors[] = ['danger' => 'Account ID is required'];
        } else {
            $account = $this->accountRepo->find($accountId);
            if (!$account instanceof Account) {
                $errors[] = ['danger' => 'Invalid account ID specified'];
            } else {
                $_account = $user->getAccount();
                if (!$_account instanceof Account) {
                    // Creating a user won't have an account yet
                    $user->setAccount($account);
                } elseif ($user->getAccount()->getId() !== $accountId) {
                    // If we're updating the accountId then we're moving the user to another account
                    $user->setAccount($account);
                }
            }
        }

        $allowedDomains = $account->getAllowedDomains();

        if ($user->getEmail() != $email) {
            // Check to make sure it's a valid email address format
            $result = $this->validateEmail($email);

            if (!empty($result['error'])) {
                $errors[] = ['danger' => $result['error']];
            
            } elseif (!empty($allowedDomains)) {    
                // Make sure the email is an allowed domain
                $emailParts = explode('@', $email);
                $domains    = explode(trim(','), $allowedDomains);
                
                if (!in_array($emailParts[1], $domains)) {
                    $errors[] = ['danger' => 'Only these domains are allowed: ' . $allowedDomains];
                } else {
                    $user->setEmail($email);
                }

            } else {
                $user->setEmail($email);
            }
        }

        if (!empty($firstName)) {
            $user->setFirstName($firstName);
        }

        if (!empty($lastName)) {
            $user->setLastName($lastName);
        }

        return [
            'errors' => $errors,
            'user'   => $user
        ];
    }

    /**
     * @param string $email
     * @return array $errors
     */
    public function validateUsername(string $username)
    {
        $result     = ['success' => null, 'error' => null];
        $minLength  = $this->params->get('username_min_length');

        if (empty($username) || \mb_strlen($username) < $minLength) {
            $result['error'] = 'Username must be at least ' . $minLength . ' characters';
        } else {
            $u = $this->userRepo->findOneByUsername($username);
        
            if ($u instanceof User) {
                $result['error'] = 'Username is already taken';
            } else {
                $result['success'] = 'Username is available';
            }
        }

        return $result;
    }

    /**
     * @param string $email
     * @return array $errors
     */
    public function validateEmail(string $email)
    {
        $result = ['success' => null, 'error' => null];

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $result['error'] = 'Invalid email address format';
        } else {
            $u = $this->userRepo->findOneByEmail($email);
        
            if ($u instanceof User) {
                $result['error'] = 'Email cannot be used';
            } else {
                $result['success'] = 'Email is valid';
            }
        }

        return $result;
    }
}