<?php

namespace App\Entity;

use App\Repository\AccountRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;

/**
 * @ORM\Entity(repositoryClass=AccountRepository::class)
 */
class Account
{
    /**
     * @var \Ramsey\Uuid\UuidInterface
    *
    * @ORM\Id()
    * @ORM\Column(type="uuid", unique=true)
    * @ORM\GeneratedValue(strategy="CUSTOM")
    * @ORM\CustomIdGenerator(class=UuidGenerator::class)
    */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="account")
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $users;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDeleted;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $allowedDomains;

    /**
     * @ORM\OneToMany(targetEntity=AccountNote::class, mappedBy="account", orphanRemoval=true)
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $accountNotes;

    /**
     * @ORM\OneToMany(targetEntity=AccountFeature::class, mappedBy="account", orphanRemoval=true)
     */
    private $accountFeatures;

    /**
     * @ORM\OneToMany(targetEntity=ApiKey::class, mappedBy="account", orphanRemoval=true)
     */
    private $apiKeys;

    public function __construct()
    {
        $this->users        = new ArrayCollection();
        $this->createdAt    = new \DateTime();
        $this->isDeleted    = false;
        $this->accountNotes = new ArrayCollection();
        $this->accountFeatures = new ArrayCollection();
        $this->apiKeys = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setAccount($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getAccount() === $this) {
                $user->setAccount(null);
            }
        }

        return $this;
    }

    public function getIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getAllowedDomains(): ?string
    {
        return $this->allowedDomains;
    }

    public function setAllowedDomains(?string $allowedDomains): self
    {
        $this->allowedDomains = $allowedDomains;

        return $this;
    }

    /**
     * @return Collection|AccountNote[]
     */
    public function getAccountNotes(): Collection
    {
        return $this->accountNotes;
    }

    public function addAccountNote(AccountNote $accountNote): self
    {
        if (!$this->accountNotes->contains($accountNote)) {
            $this->accountNotes[] = $accountNote;
            $accountNote->setAccount($this);
        }

        return $this;
    }

    public function removeAccountNote(AccountNote $accountNote): self
    {
        if ($this->accountNotes->contains($accountNote)) {
            $this->accountNotes->removeElement($accountNote);
            // set the owning side to null (unless already changed)
            if ($accountNote->getAccount() === $this) {
                $accountNote->setAccount(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AccountFeature[]
     */
    public function getAccountFeatures(): Collection
    {
        return $this->accountFeatures;
    }

    public function addAccountFeature(AccountFeature $accountFeature): self
    {
        if (!$this->accountFeatures->contains($accountFeature)) {
            $this->accountFeatures[] = $accountFeature;
            $accountFeature->setAccount($this);
        }

        return $this;
    }

    public function removeAccountFeature(AccountFeature $accountFeature): self
    {
        if ($this->accountFeatures->contains($accountFeature)) {
            $this->accountFeatures->removeElement($accountFeature);
            // set the owning side to null (unless already changed)
            if ($accountFeature->getAccount() === $this) {
                $accountFeature->setAccount(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ApiKey[]
     */
    public function getApiKeys(): Collection
    {
        return $this->apiKeys;
    }

    public function addApiKey(ApiKey $apiKey): self
    {
        if (!$this->apiKeys->contains($apiKey)) {
            $this->apiKeys[] = $apiKey;
            $apiKey->setAccount($this);
        }

        return $this;
    }

    public function removeApiKey(ApiKey $apiKey): self
    {
        if ($this->apiKeys->contains($apiKey)) {
            $this->apiKeys->removeElement($apiKey);
            // set the owning side to null (unless already changed)
            if ($apiKey->getAccount() === $this) {
                $apiKey->setAccount(null);
            }
        }

        return $this;
    }
}
