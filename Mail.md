# Mail

## Symfony Mailer Documentation
- Symfony has documentation that supports
    - Amazon SES, Gmail, MailChimp, Mailgun, Postmark, & Sendgrid.
    - You can also use any SMTP service
    - See details https://symfony.com/doc/current/mailer.html

## SendGrid
I recommend using SendGrid, super easy setup and a free tier. That's what I have setup in this code base.
- https://sendgrid.com/
- After sign-up, and login - on the left navigation 
    - Click Email API 
    - Then click Integration Guide
    - Click on the `Web API` to start the wizard
    - Click `PHP`
    - Create a key and put the value in your `.env` file.
    - The existing `MailService` code will be able to send emails now.
    

# Using another provider
You'll need to edit your `.env` file, `config/services.yml` file, and adjust the `App\Service\MailerService` to leverage another provider. You'll also want to remove sendgrid packages.
```
$ composer remove sendgrid/sendgrid
```